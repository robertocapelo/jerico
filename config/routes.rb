Nomade::Application.routes.draw do
  require 'subdomain' 

#  get "denuncia/new"

#  get "denuncia/create"
#  resources :denuncia
  

  resources :fotos
  resources :imobiliaria

  resources :categoria_placas

  resources :sessions, :only => [:new, :create, :destroy, :opcoes]
  match '/login', :to => 'sessions#new'
  match '/logout', :to => 'sessions#destroy'
  match '/opcoes', :to => 'sessions#opcoes'

  resources :usuarios
  resources :tipo_placas
  match '/placas/fotos_placa/' => 'placas#fotos_placa', :as => "fotos_placa", :via => [:get]
  match '/placas/list' => 'placas#list', :as => "list", :via => [:get]
  
  match '/placas/infogrupo' => "placas#infogrupo", :as => "infogrupo", :via => [:get]
  
  resources :interesse, :only => [:index]

  resources :placas do
    member do
      get :new_interesse
      get :destroy_interesse
    end
    
    resources :denuncia, :only => [:new, :create]
    
    resources :compartilha, :only => [:new, :create]
    resources :interesse, :only => [:new, :create]
    match "interesse/removerlista", :to => "interesse#destroy", :as =>"removerlista"

    match "info"
    match "detalhe"
    match "edit", :to => "placas#edit"
    match ":desabilita", :to => "placas#destroy", :as =>"desabilita"
  end
  #match "/info" => "projects#info", :as => "info"
  

  # match "/placas/list", :via => [:get]

  # match 'placas/list' => 'placas#list', :as => :list
  
  constraints(Subdomain) do
    match '/' => 'placas#index'
  end
  
  root :to => "placas#index"

  get 'filtros' => 'placas#filtros' 
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action


  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
