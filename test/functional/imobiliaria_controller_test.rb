require 'test_helper'

class ImobiliariaControllerTest < ActionController::TestCase
  setup do
    @imobiliarium = imobiliaria(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:imobiliaria)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create imobiliarium" do
    assert_difference('Imobiliarium.count') do
      post :create, imobiliarium: @imobiliarium.attributes
    end

    assert_redirected_to imobiliarium_path(assigns(:imobiliarium))
  end

  test "should show imobiliarium" do
    get :show, id: @imobiliarium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @imobiliarium
    assert_response :success
  end

  test "should update imobiliarium" do
    put :update, id: @imobiliarium, imobiliarium: @imobiliarium.attributes
    assert_redirected_to imobiliarium_path(assigns(:imobiliarium))
  end

  test "should destroy imobiliarium" do
    assert_difference('Imobiliarium.count', -1) do
      delete :destroy, id: @imobiliarium
    end

    assert_redirected_to imobiliaria_path
  end
end
