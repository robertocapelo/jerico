require 'test_helper'

class TipoPlacasControllerTest < ActionController::TestCase
  setup do
    @tipo_placa = tipo_placas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_placas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_placa" do
    assert_difference('TipoPlaca.count') do
      post :create, tipo_placa: @tipo_placa.attributes
    end

    assert_redirected_to tipo_placa_path(assigns(:tipo_placa))
  end

  test "should show tipo_placa" do
    get :show, id: @tipo_placa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_placa
    assert_response :success
  end

  test "should update tipo_placa" do
    put :update, id: @tipo_placa, tipo_placa: @tipo_placa.attributes
    assert_redirected_to tipo_placa_path(assigns(:tipo_placa))
  end

  test "should destroy tipo_placa" do
    assert_difference('TipoPlaca.count', -1) do
      delete :destroy, id: @tipo_placa
    end

    assert_redirected_to tipo_placas_path
  end
end
