require 'test_helper'

class CategoriaPlacasControllerTest < ActionController::TestCase
  setup do
    @categoria_placa = categoria_placas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:categoria_placas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create categoria_placa" do
    assert_difference('CategoriaPlaca.count') do
      post :create, categoria_placa: @categoria_placa.attributes
    end

    assert_redirected_to categoria_placa_path(assigns(:categoria_placa))
  end

  test "should show categoria_placa" do
    get :show, id: @categoria_placa
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @categoria_placa
    assert_response :success
  end

  test "should update categoria_placa" do
    put :update, id: @categoria_placa, categoria_placa: @categoria_placa.attributes
    assert_redirected_to categoria_placa_path(assigns(:categoria_placa))
  end

  test "should destroy categoria_placa" do
    assert_difference('CategoriaPlaca.count', -1) do
      delete :destroy, id: @categoria_placa
    end

    assert_redirected_to categoria_placas_path
  end
end
