class PlacasController < ApplicationController
  skip_before_filter :login_required, :only => [:filtros, :index, :show, :info ,:fotos_placa, :detalhe, :infogrupo, :new_interesse, :destroy_interesse]
  before_filter :imobiliaria_required, :only => [:new, :edit]

  def filtros
    
    filtros = []
    quartos = []
    tipos = []
    categorias = []

    qx = []
    qts =[]
    q = []
    itens = []

    categorias = Placa.select("categoria_placas.id, categoria_placas.desc ")
                            .joins(:categoria_placa)
                            .group("categoria_placa_id")
    
    # --> aluga/apto --> q1/q2

    #[aluga/vende]
    #filtro[0].aluga
       #[apto/casa]
       #filtro[0].aluga[0].apto
          #[q1/q2]
          #filtro[0].aluga[0].apto[0].q1[0][0]
          #filtro[0].aluga[0].apto[0].q1[0][1]
          #filtro[0].aluga[0].apto[1].q2[0][0]
          #filtro[0].aluga[0].apto[1].q2[0][1]
       #filtro[0].aluga[1].casa
          #filtro[0].aluga[1].casa[0].q1[0][0]
          #filtro[0].aluga[1].casa[0].q1[0][1]
          #filtro[0].aluga[1].casa[1].q2[0][0]
          #filtro[0].aluga[1].casa[1].q2[0][1]
    #filtro[1].vende
       #filtro[1].aluga[0].apto
          #filtro[1].aluga[0].apto[0].q1[0][0]
          #filtro[1].aluga[0].apto[0].q1[0][1]
          #filtro[1].aluga[0].apto[1].q2[0][0]
          #filtro[1].aluga[0].apto[1].q2[0][1]
       #filtro[1].aluga[1].casa
          #filtro[1].aluga[1].casa[0].q1[0][0]
          #filtro[1].aluga[1].casa[0].q1[0][1]
          #filtro[1].aluga[1].casa[1].q2[0][0]
          #filtro[1].aluga[1].casa[1].q2[0][1]

    
    categorias.each{|cat|    

      tipos = TipoPlaca.where('tipo_placas.id in (select tipo_placa_id from placas where categoria_placa_id = ?)',cat.id)

      tipos.each{|tipo|
        
        quartos = Placa.where(:categoria_placa_id => cat.id, :tipo_placa_id => tipo.id, :desabilitado => true).group(:quartos)

        quartos.each{|quarto|
          qx << Placa.where(:categoria_placa_id => cat.id, :tipo_placa_id => tipo.id,:quartos => quarto.quartos, :desabilitado => true)
          q << {'q'+quarto.quartos.to_s => qx.clone}
          qx = []
        }

        qts << {tipo.desc => q.clone}
        q = []
      }


      
      itens << {cat.desc => qts.clone} 
      
      filtros = {"filtros" => itens}

      quartos = []
      qts = []
    }

    render json: filtros.to_json
  end

  def destroy_interesse
    puts "CHAMADO DESTROY"

    lista_interesse_temp = cookies['interesse']
    interesse = params[:id].to_s
    
    if lista_interesse_temp != nil
      placas = lista_interesse_temp.split("-")
      placas.delete(interesse)

      cookies.permanent['interesse'] = { :value => placas.join("-"), :expires => 48.hour.from_now}
    end
    
    respond_to do |format|
      format.json { render json: @placa }
    end

  end

  def new_interesse
    puts "CHAMADO ADD"

    lista_interesse_temp = cookies['interesse']
    interesse = params[:id].to_s
    #interesse = placa.id.to_s
    puts "plca: "+interesse
    if lista_interesse_temp != nil
      
      placas = lista_interesse_temp.split("-").uniq
      if !placas.include?(interesse)
        lista_interesse_temp += "-" 
        lista_interesse_temp += interesse
        
        cookies.permanent['interesse'] = { :value => lista_interesse_temp, :expires => 48.hour.from_now}
        puts "LISTAINTERESSE : "+interesse
        puts "--> "+cookies['interesse']
      end
    else

      lista_interesse_temp = "" 
      cookies.permanent['interesse'] = { :value => interesse, :expires => 48.hour.from_now}
      
      puts "LISTAINTERESSE : "+interesse
      puts "--> "+cookies['interesse']
    end
    #link_to_function(name, "atualiza_mapa()")
    
    respond_to do |format|
      format.json { render json: @placa }
    end

  end

  def infogrupo
    
    grupoid = []

    params[:infogrupo].split(",").each do |id|
      grupoid.push(id)
    end

    @placas = Placa.where("id in (?)", grupoid)
    
    respond_to do |format|
      format.html # infogrupo.html.erb
      format.json { render json: @placa }
    end

  end

  def info
    @placa = Placa.find(params[:placa_id])

    respond_to do |format|
      format.html # info.html.erb
      format.json { render json: @placa }
    end
  end

  # GET /placas/fotos_placa
  # GET /placas/fotos_placa.json
  def fotos_placa
    @placa = Placa.find(params[:placa_id])

    respond_to do |format|
      format.html # fotos_placa.html.erb
      format.json { render json: @placa }
    end
  end

  # GET /placas/list
  # GET /placas/list.json
  def list
    @placas = Placa.where("imobiliarium_id = (?)",@current_usuario.imobiliarium)

    respond_to do |format|
      format.html # list.html.erb
      format.json { render json: @placas }
    end
  end
    
  # GET /placas
  # GET /placas.json
  def index
    
    if request.subdomain != '' && request.subdomain != nil
      
      formatedrequest = request.subdomain.split('.')[0]

      if formatedrequest != 'www' && formatedrequest != 'nomade'
        @imobiliaria = Imobiliarium.find_by_nome!(formatedrequest);
      end
    end

    filtrar = Hash.new("filtro")
    if params[:quartos] != nil
      filtrar["quartos"] = params[:quartos]
    end
    if params[:valor] != nil    
      filtrar["valor"] = params[:valor]
    end 
    if params[:tipo] != nil    
      filtrar["tipo_placa_id"] = params[:tipo]
    end 
    if params[:categoria] != nil    
      filtrar["categoria_placa_id"] = params[:categoria]
    end 
    if params[:mostrar] != nil    
      filtrar["imobiliarium_id"] = params[:mostrar]
    end 

    sqlquery = " "
    
    filtrar.each_with_index do |(key, value),index|
     
        operador = " = "
        if key.to_s == "valor"  
          operador = " <= "
        end

        if index > 0
          sqlquery = sqlquery +" and "+ key.to_s + operador + value.to_s
        else
          sqlquery = sqlquery +" "+ key.to_s + operador + value.to_s
        end

    end

   
    if filtrar.empty?
     # @placas = Placa.all
      if @imobiliaria != nil
        @placas = Placa.where("desabilitado = 1 and imobiliarium_id = #{@imobiliaria.id}")
      else
        @placas = Placa.where("desabilitado = 1")
      end
    else
      if @imobiliaria != nil
        @placas = Placa.where(sqlquery+ " and desabilitado = 1 and imobiliarium_id = #{@imobiliaria.id}")
      else
        @placas = Placa.where(sqlquery+ " and desabilitado = 1")
      end
    end 

    #@json = @placas.to_gmaps4rails

    @json = @placas.to_gmaps4rails do |placa, marker|
      lista_interesse = cookies['interesse']
      if lista_interesse != nil
        lista_interesse = lista_interesse.split("-").uniq
        if lista_interesse.include?(placa.id.to_s)
          placa.categoria_placa.image = placa.categoria_placa.image.split(".")[0]+"interesse.png"
        end  
      end
      marker.json({ :id => placa.id })
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @json }
    end
  end

  # GET /placas/1
  # GET /placas/1.json
  def show
    @placa = Placa.find(params[:id])

    @json = @placa.to_gmaps4rails

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @json }
    end
  end

  def detalhe
    @placa = Placa.find(params[:placa_id])

    respond_to do |format|
      format.html
      format.json { render json: @placa }
    end
  end

  # GET /placas/new
  # GET /placas/new.json
  def new
    @placa = Placa.new
    @placa.tipo = 'default'
    @placa.longitude = '-46.632982'
    @placa.latitude = '-23.550602'
    @placa.categoria_placa = CategoriaPlaca.new
    @placa.valor = '0'
    @placa.condominio = '0'
    @placa.vagas = 0
    @placa.quartos = 1
    @json = @placa.to_gmaps4rails

    3.times { @placa.observacaos.build }

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @placa }
    end
  end

  # GET /placas/1/edit
  def edit
    @placa = Placa.find(params[:placa_id])
    @json = @placa.to_gmaps4rails
  end

  # POST /placas
  # POST /placas.json
  def create
    @placa = Placa.new(params[:placa])
    @placa.imobiliarium = @current_usuario.imobiliarium

    respond_to do |format|
      if @placa.save
        #format.html { redirect_to :controller => 'placas' }
        format.html { redirect_to @placa, notice: 'A Placa foi colocada com sucesso.' }
        format.json { render json: @placa, status: :created, location: @placa }
      else
        @json = @placa.to_gmaps4rails
        format.html { render action: "new" }
        format.json { render json: @json.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /placas/1
  # PUT /placas/1.json
  def update
    @placa = Placa.find(params[:id])

    respond_to do |format|
      if @placa.update_attributes(params[:placa])
        format.html { redirect_to @placa, notice: 'A Placa foi modificada com sucesso.' }
        format.json { head :no_content }
        
      else
        format.html { render action: "edit" }
        format.json { render json: @placa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /placas/1
  # DELETE /placas/1.json
  def destroy

    @placa = Placa.find(params[:placa_id])
    #@placa.destroy

    @placa.desabilitado = params[:desabilita];
    @placa.save

    respond_to do |format|
      format.html { redirect_to list_path }
      format.json { head :no_content }
    end
  end
end
