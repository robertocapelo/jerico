class CategoriaPlacasController < ApplicationController
  # GET /categoria_placas
  # GET /categoria_placas.json
  def index
    @categoria_placas = CategoriaPlaca.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @categoria_placas }
    end
  end

  # GET /categoria_placas/1
  # GET /categoria_placas/1.json
  def show
    @categoria_placa = CategoriaPlaca.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @categoria_placa }
    end
  end

  # GET /categoria_placas/new
  # GET /categoria_placas/new.json
  def new
    @categoria_placa = CategoriaPlaca.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @categoria_placa }
    end
  end

  # GET /categoria_placas/1/edit
  def edit
    @categoria_placa = CategoriaPlaca.find(params[:id])
  end

  # POST /categoria_placas
  # POST /categoria_placas.json
  def create
    @categoria_placa = CategoriaPlaca.new(params[:categoria_placa])

    respond_to do |format|
      if @categoria_placa.save
        format.html { redirect_to @categoria_placa, notice: 'Categoria placa was successfully created.' }
        format.json { render json: @categoria_placa, status: :created, location: @categoria_placa }
      else
        format.html { render action: "new" }
        format.json { render json: @categoria_placa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /categoria_placas/1
  # PUT /categoria_placas/1.json
  def update
    @categoria_placa = CategoriaPlaca.find(params[:id])

    respond_to do |format|
      if @categoria_placa.update_attributes(params[:categoria_placa])
        format.html { redirect_to @categoria_placa, notice: 'Categoria placa was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @categoria_placa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categoria_placas/1
  # DELETE /categoria_placas/1.json
  def destroy
    @categoria_placa = CategoriaPlaca.find(params[:id])
    @categoria_placa.destroy

    respond_to do |format|
      format.html { redirect_to categoria_placas_url }
      format.json { head :no_content }
    end
  end
end
