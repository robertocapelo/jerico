class CompartilhaController < ApplicationController
  skip_before_filter :login_required, :only => [:new, :create]
  
  def new
    @placa = Placa.find(params[:placa_id])
  	respond_to do |format|
      format.html # new.html.erb
    end
  end

  def create
    @compartilha = Hash.new
    @compartilha['nome'] = params[:nome]
    @compartilha['email'] = params[:email]
    @compartilha['destinatario'] = params[:destinatario]
    @compartilha['emaildestinatario'] = params[:emaildestinatario]

    @placa = Placa.find(params[:placa_id])
    
  	respond_to do |format|
      Sendemail.compartilha_mail(@compartilha, @placa).deliver
      format.html { redirect_to placas_path }
    end
  end
end
