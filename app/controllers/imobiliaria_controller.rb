class ImobiliariaController < ApplicationController


  # GET /imobiliaria/1
  # GET /imobiliaria/1.json
  def show
    @imobiliarium = Imobiliarium.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @imobiliarium }
    end
  end

  # GET /imobiliaria/new
  # GET /imobiliaria/new.json
  def new
    @imobiliarium = Imobiliarium.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @imobiliarium }
    end
  end

  # GET /imobiliaria/1/edit
  def edit
    @imobiliarium = Imobiliarium.find(params[:id])
  end

  # POST /imobiliaria
  # POST /imobiliaria.json
  def create
    @imobiliarium = Imobiliarium.new(params[:imobiliarium])
    @imobiliarium.usuario = @current_usuario

    respond_to do |format|
      if @imobiliarium.save
        format.html { redirect_to @imobiliarium, notice: 'A imobiliaria cadastrada com sucesso.' }
        format.json { render json: @imobiliarium, status: :created, location: @imobiliarium }
      else
        format.html { render action: "new" }
        format.json { render json: @imobiliarium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /imobiliaria/1
  # PUT /imobiliaria/1.json
  def update
    @imobiliarium = Imobiliarium.find(params[:id])
    
    respond_to do |format|
      if @imobiliarium.update_attributes(params[:imobiliarium])
        format.html { redirect_to @imobiliarium, notice: 'A imobiliaria foi modificada com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @imobiliarium.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /imobiliaria/1
  # DELETE /imobiliaria/1.json
  def destroy
    @imobiliarium = Imobiliarium.find(params[:id])
    @imobiliarium.destroy

    respond_to do |format|
      format.html { redirect_to imobiliaria_url }
      format.json { head :no_content }
    end
  end
end
