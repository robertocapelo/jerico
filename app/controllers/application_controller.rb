class ApplicationController < ActionController::Base
  before_filter :login_required
  before_filter :set_current_usuario

  layout Proc.new { |controller| controller.request.xhr? ? nil : 'application' }
  #protect_from_forgery

  private
  def autorizado?
  	session[:usuario_id] || controller_name=="sessions"
  end

  def login_required
    #puts new_placa_path
    autorizado? || acesso_negado
  end

  def imobiliaria_required
    if @current_usuario.imobiliarium == nil
      respond_to do |format|
        format.html do
          session[:return_to] = request.url
          redirect_to(new_imobiliarium_path, :remote => true)
        end
      end      
    end
  end

  def acesso_negado
  	respond_to do |format|
  		format.html() do
  			session[:return_to] = request.url
        redirect_to login_path, :action => "new" 
  		end
  	end
  end

  def set_current_usuario
    if session[:usuario_id]
      @current_usuario = Usuario.find session[:usuario_id]
    end
    true
  end

  def redirect_to(options = {}, response_status = {})
    if request.xhr?
      render(:update) {|page| page.redirect_to(options)}
    else
      super(options, response_status)
    end
  end

end
