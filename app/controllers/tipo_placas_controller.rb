class TipoPlacasController < ApplicationController
  # GET /tipo_placas
  # GET /tipo_placas.json
  def index
    @tipo_placas = TipoPlaca.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tipo_placas }
    end
  end

  # GET /tipo_placas/1
  # GET /tipo_placas/1.json
  def show
    @tipo_placa = TipoPlaca.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tipo_placa }
    end
  end

  # GET /tipo_placas/new
  # GET /tipo_placas/new.json
  def new
    @tipo_placa = TipoPlaca.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tipo_placa }
    end
  end

  # GET /tipo_placas/1/edit
  def edit
    @tipo_placa = TipoPlaca.find(params[:id])
  end

  # POST /tipo_placas
  # POST /tipo_placas.json
  def create
    @tipo_placa = TipoPlaca.new(params[:tipo_placa])

    respond_to do |format|
      if @tipo_placa.save
        format.html { redirect_to @tipo_placa, notice: 'Tipo placa was successfully created.' }
        format.json { render json: @tipo_placa, status: :created, location: @tipo_placa }
      else
        format.html { render action: "new" }
        format.json { render json: @tipo_placa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tipo_placas/1
  # PUT /tipo_placas/1.json
  def update
    @tipo_placa = TipoPlaca.find(params[:id])

    respond_to do |format|
      if @tipo_placa.update_attributes(params[:tipo_placa])
        format.html { redirect_to @tipo_placa, notice: 'Tipo placa was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tipo_placa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_placas/1
  # DELETE /tipo_placas/1.json
  def destroy
    @tipo_placa = TipoPlaca.find(params[:id])
    @tipo_placa.destroy

    respond_to do |format|
      format.html { redirect_to tipo_placas_url }
      format.json { head :no_content }
    end
  end
end
