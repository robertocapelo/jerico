class SessionsController < ApplicationController
	skip_before_filter :login_required, :only => [:new, :create]

	def new
		if !params[:modal]
			@modal = 1
		end
	end
	
	def create
		@usuario = Usuario.logon(params[:login], params[:senha])
		if @usuario
			
			session[:usuario_id] = @usuario.id

			respond_to do |format|
		      format.html { redirect_to :controller => 'placas', :minhas => 1 }
		    end
		else
			flash[:notice] = "O login falhou, verifique o nome de usuario e senha informados."
			redirect_to login_path, :action => "new" 
		end
	end
   
	def destroy
		session[:usuario_id] = nil
		@current_usuario = nil

		redirect_to root_url
	end

    def opcoes
      respond_to do |format|
        format.html
	  end
    end

end
