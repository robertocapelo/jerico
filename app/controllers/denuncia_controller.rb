class DenunciaController < ApplicationController
  skip_before_filter :login_required, :only => [:new, :create]
  
  def new
  	@placa = Placa.find(params[:placa_id])
  	respond_to do |format|
      format.html # new.html.erb
    end
  end

  def create
  	@denuncia = params[:descricao]
    @placa = Placa.find(params[:placa_id])
  	respond_to do |format|
      Sendemail.denuncio_mail(@denuncia, @placa).deliver
      format.html { redirect_to placas_path }
    end
  end
end
