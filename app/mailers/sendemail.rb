class Sendemail < ActionMailer::Base
  default from: "nomade@nomade.net.br"

  def denuncio_mail(denuncio, placa)   
  	@denuncio = denuncio
  	@placa = placa
    mail(:To => "nomade@nomade.net.br", :subject => "Placa denunciada")  
  end

  def compartilha_mail(compartilha, placa)   
    @compartilha = compartilha
  	@placa = placa
    mail(:To => compartilha['emaildestinatario'], :subject => "Uma placa foi compartilhada!")  
  end

end
