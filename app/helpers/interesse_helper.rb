module InteresseHelper

  def link_to_add_interesse(name)
    link_to_function(name, "atualiza_mapa(0)")
  end

  def link_to_rmv_interesse(name)
	  link_to_function(name, "atualiza_mapa(1)")
  end

end
