module PlacasHelper

	def pegatipos
		@tipos = TipoPlaca.find(:all).collect{|t| [t.desc, t.id]}
    	@tipos.delete_at(0)
    	@tipos
	end

	def pegacategorias
		@categorias = CategoriaPlaca.find(:all).collect{|t| [t.desc, t.id]}
    	@categorias.delete_at(0)
    	@categorias
	end

	def pegaquartos
		[["todos","todos"],["1",1],["2",2],["3",3],["4",4],["5",5]]
	end

	def pegaendereco
      if !@placa.ocultar?
        @placa.endereco
      else
        "oculto"
      end
    end

  def listainteresse(placa)
    lista_interesse_temp = cookies['interesse']
	  
    if lista_interesse_temp != nil
	    placas = lista_interesse_temp.split("-").uniq
	      if !placas.include?(placa.id.to_s)
	        link_to_add_interesse "Adicionar a lista de interesse"
        else
          link_to_rmv_interesse "Remover da lista de interesse"
	      end
    else
      link_to_add_interesse "Adicionar a lista de interesse"
    end
  end

end
