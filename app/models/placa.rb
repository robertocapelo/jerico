# encoding: utf-8

class Placa < ActiveRecord::Base
	acts_as_gmappable :msg => "Endereço não valido"
	
	belongs_to :tipo_placa
	belongs_to :categoria_placa
	belongs_to :imobiliarium

	has_many :observacaos
	accepts_nested_attributes_for :observacaos, :reject_if => lambda { |a| a[:conteudo].blank? }, :allow_destroy => true
	
	has_many :fotos

	validates_presence_of :desc, :message => " - É um campo obrigatorio."
	validates_presence_of :endereco, :message => " - Digite e/ou arraste o marcador para onde deseja colocar a placa."
	validates_presence_of :valor, :message => " - Não pode ficar em branco."
	validates :valor, :numericality => {:greater_than => 200, :message => "- Deve ser maior que o indicado."}
	validates :valor, :numericality => {:less_than => 1000000, :message => "- Deve ser menor que o indicado."}

	#validates_length_of :obs, :in => 0..3000, :message => " - Esse campo deve ter até 3000 caracteres"

	usar_como_dinheiro :valor
	usar_como_dinheiro :condominio




	def gmaps4rails_address 
		endereco
	end

	def gmaps4rails_infowindow

    end
    
    def gmaps4rails_title
		endereco
    end

	def gmaps4rails_marker_picture

	    if self.categoria_placa.image != nil
    		if self.valid?
    			{
    			"picture" => self.categoria_placa.image,
				"width" => 60,          # string, mandatory
				"height" => 60,          # string, mandatory
				#"marker_anchor" => ,   # array, facultative
				"shadow_picture" => "/images/shadow.png",  # string, facultative
				"shadow_width" => 60,    # string, facultative
				"shadow_height" => 60,   # string, facultative
				#"shadow_anchor" => ,   # string, facultative
				}
			else
		    	{
		    	"picture" => "/images/marker.png",
				"width" => 50,          # string, mandatory
				"height" => 50,          # string, mandatory
				#"marker_anchor" => ,   # array, facultative
				#"shadow_picture" => ,  # string, facultative
				#"shadow_width" => ,    # string, facultative
				#"shadow_height" => ,   # string, facultative
				#"shadow_anchor" => ,   # string, facultative
				}
			end
		else
			{
	    	"picture" => "/images/marker.png",
			"width" => 50,          # string, mandatory
			"height" => 50,          # string, mandatory
			#"marker_anchor" => ,   # array, facultative
			#"shadow_picture" => ,  # string, facultative
			#"shadow_width" => ,    # string, facultative
			#"shadow_height" => ,   # string, facultative
			#"shadow_anchor" => ,   # string, facultative
			}
		end
	end



end
