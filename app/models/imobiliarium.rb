# encoding: utf-8

class Imobiliarium < ActiveRecord::Base
  belongs_to :usuario
  has_many :placa
  usar_como_cnpj :cnpj

  attr_accessible :imagem, :nome, :cnpj, :site, :email, :telefone, :endereco
  mount_uploader :imagem, ImagemUploader
  
  validates_presence_of :telefone, :nome, :cnpj, :endereco, :message => " - É um campo obrigatorio."

  validates_uniqueness_of :cnpj, :message => " - Já foi cadastrado."
  validates_format_of :site, :with => /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix
  validates_format_of :email, :with => /^(|(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6})$/i
end
