# encoding: utf-8

class Usuario < ActiveRecord::Base
	
	has_one :imobiliarium

	validates_presence_of :login, :senha, :message => " - Esse campo e obrigatorio"
	validates_length_of :login, :in => 5..20, :message => " - Esse campo deve ter de 5 a 20 caracteres"
	validates_presence_of :senha, :in => 5..10, :message => " - Esse campo deve ter de 5 a 10 caracteres"
	validates_uniqueness_of :login, :message => " - Login já existe."
	validates_confirmation_of :senha, :message => ' - Não está igual ao campo de confirmação de senha'

	def self.logon(login, senha)
		usuario = Usuario.first :conditions => ["login = ? and senha = ?", login, senha]
		usuario
	end
end
