# encoding: utf-8

class Foto < ActiveRecord::Base
  belongs_to :placa

  attr_accessible :descricao, :imagem, :placa_id
  mount_uploader :imagem, ImagemUploader

  validates_presence_of :descricao, :imagem, :message => " - É um campo obrigatorio."
end
