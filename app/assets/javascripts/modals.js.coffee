$ ->
  $('.denuncia-close').live 'click', () -> 
    $('#denuncia-modal').hide()
    $('#denuncia-modal-container').hide()

  $('.compartilha-close').live 'click', () -> 
    $('#center-modal').hide()
    $('#center-modal-container').hide()

  $('.center-close').live 'click', () -> 
    
    currentMarkerSel = Gmaps.map.markers[currentsel.value]
    if currentMarkerSel != null
      currentMarkerSel.serviceObject.setAnimation(null);

    $('#center-modal').hide()
    $('#center-modal-container').hide()
    $('#foto-modal').hide()
    $('#foto-modal-container').hide()
    $('#modal').hide()
    $('#modal-container').hide()

  $('.close').live 'click', () -> 
    
    currentMarkerSel = Gmaps.map.markers[currentsel.value]
    if currentMarkerSel != null
      currentMarkerSel.serviceObject.setAnimation(null);

    $('#center-modal').hide()
    $('#center-modal-container').hide()
    $('#foto-modal').hide()
    $('#foto-modal-container').hide()
    $('#modal').hide()
    $('#modal-container').hide()

  $('.foto-close').live 'click', () -> 
    $('#foto-modal').hide()
    $('#foto-modal-container').hide()

  $('.showfotos').live 'click', (status,data,xhr) -> 
    $.ajax  
      url: "placas/fotos_placa?placa_id="+$('#atual').val(),  
      success: (xhr, status, data) ->
        $modal_header_fotos = $('#foto-modal').find('.header_info_fotos')

        $('#foto-modal').html(xhr).prepend($modal_header_fotos).css('top', $('#mapa').position().top + 85).show()
        $('#mycarousel').jcarousel()
      $('#foto-modal-container').show()

  $('.showdenuncia').live 'click', (status, data, xhr) ->
    $.ajax  
      url: "placas/"+$('#atual').val()+"/denuncia/new",  
      success: (xhr, status, data) ->
        $center_info_denuncia = $('#denuncia-modal').find('.header_info_denuncia')
        $('#denuncia-modal')
          .html(xhr)
          .prepend($center_info_denuncia)
          .css('top', $(window).height()/2-$('#denuncia-modal').height()/2)
          .show()
        $('#denuncia-modal-container').show();


  $('#menu').hover () ->
    $('#opcoes-modal').fadeIn()
    # $('#opcoes-modal-container').show()
      
  # $('#menu').live 'hover', (status, data, xhr) ->
    # $('#opcoes-modal-container').show()
    
    # $.ajax  
    #   url: "opcoes",  
    #   success: (xhr, status, data) ->
    #     $center_info_opcoes = $('#opcoes-modal').find('.header_info_opcoes')
    #     $('#opcoes-modal')
    #       .html(xhr)
    #       .prepend($center_info_opcoes)
    #       .fadeIn()
    #     $('#opcoes-modal-container').show();

  #$('#opcoes-modal').live 'hover', (status, data, xhr) ->
    #$("#opcoes-modal").hide();
    #opcoesx = document.getElementById('opcoes-modal');
    #if opcoesx.hidden
    #  console.log 'sumir'
    #else
    #  console.log 'aparecer'

  

  $('.showcompartilha').live 'click', (status, data, xhr) ->
    $.ajax  
      url: "placas/"+$('#atual').val()+"/compartilha/new",  
      success: (xhr, status, data) ->
        $center_info_compartilha = $('#center-modal').find('.header_info_compartilha')
        $('#center-modal')
          .html(xhr)
          .prepend($center_info_compartilha)
          .css('top', $(window).height()/2-$('#center-modal').height()/2)
          .show()
        $('#center-modal-container').show();
        nome = new LiveValidation('nome');
        nome.add( Validate.Presence );
        email = new LiveValidation('email');
        email.add( Validate.Email, { failureMessage: "Email inválido!" } );
        destinatario = new LiveValidation('destinatario');
        destinatario.add( Validate.Presence );
        emaildestinatario = new LiveValidation('emaildestinatario');
        emaildestinatario.add( Validate.Presence );
        emaildestinatario.add( Validate.Email, { failureMessage: "Email inválido!" }  );

  $('[data-remote]').bind 'ajax:success', (status, data, xhr) ->
    $center_modal_header_fotos = $('#center-modal').find('.header_info_center')
    $('#center-modal')
      .html(data)
      .prepend($center_modal_header_fotos)
      .css('top', $(window).height()/2-$('#center-modal').height()/2)
      .show()
    $('#center-modal-container').show();
