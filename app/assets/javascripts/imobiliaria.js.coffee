# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  $("#imobiliarium_cnpj").mask("99.999.999/9999-99")
  $("#imobiliarium_telefone").mask("(99)9999-9999")