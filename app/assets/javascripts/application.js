// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require twitter/bootstrap
//= require_tree .
//= require modals
//= require twitter/bootstrap
//= require bootstrap

(function() {
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();

function remove_fields(link){
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".field").hide();
}  

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g");
  $(link).parent().before(content.replace(regexp, new_id));
}

function atualiza_mapa(tipo) {
   if (tipo == 0){ 
     $.ajax({
      url: "/placas/"+$('#atual').val()+"/new_interesse", 
      success: function(xhr, status, data){
        

          $.ajax({
            url: "/placas/"+$('#atual').val()+"/info", 
            success: function(xhr, status, data){
              $modal_header = $('#modal').find('.header_info');

              $('#modal').html(xhr).prepend($modal_header).css('top',  $('#mapa').position().top + 140).show();
              $('#modal-container').show();

              
              $.getJSON("/placas.json", function(data){      
                Gmaps.map.replaceMarkers(data);
                Gmaps.map.callback();

                currentMarkerSel = Gmaps.map.markers[parseInt($('#indice').val())].serviceObject
                currentMarkerSel.setAnimation(google.maps.Animation.BOUNCE);

              });
            }
          });
        
      }
     });
   }else{
     $.ajax({
      url: "/placas/"+$('#atual').val()+"/destroy_interesse", 
      success: function(xhr, status, data){
        
          $.ajax({
            url: "/placas/"+$('#atual').val()+"/info", 
            success: function(xhr, status, data){
              $modal_header = $('#modal').find('.header_info');

              $('#modal').html(xhr).prepend($modal_header).css('top',  $('#mapa').position().top + 140).show();
              $('#modal-container').show();


              $.getJSON("/placas.json", function(data){
          
                Gmaps.map.replaceMarkers(data);
                Gmaps.map.callback(); 

                currentMarkerSel = Gmaps.map.markers[parseInt($('#indice').val())].serviceObject
                currentMarkerSel.setAnimation(google.maps.Animation.BOUNCE);

              }); 
            }
          });
 
        
      }
     });
   }
}