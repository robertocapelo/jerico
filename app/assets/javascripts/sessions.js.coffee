# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$ ->

  $('.newuser').live 'click', (status,data,xhr) ->
    $.ajax 
      url: "usuarios/new", success: (xhr, status, data) ->
        $('#center-modal').hide()
        $('#center-modal-container').hide()

        $modal_close = $('#center-modal').find('.close');
        $('#center-modal').html(xhr).prepend($modal_close).css('top', $(window).height()/2-$('#center-modal').height()/2).show();

        $('#center-modal-container').show();
    return false;