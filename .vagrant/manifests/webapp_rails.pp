class webapp_rails {

  package { ['ruby1.9.3', 'libmysqlclient-dev', 'nodejs', 'build-essential', 'libxslt-dev', 'libxml2-dev', 'imagemagick', 'libmagickcore-dev', 'libmagickwand-dev' ]:
    ensure  => present,
    require => Exec['apt-get update']
  } ->

  exec { 'install bundler':
    command   => '/usr/bin/gem install bundler',
    timeout   => 0,
    logoutput => true
  } ->

  exec { 'bundle install':
    command   => '/usr/local/bin/bundle install --path=/home/vagrant',
    cwd       => '/vagrant',
    logoutput => true,
    timeout   => 0,
    user      => 'vagrant'
  } ->

  file { 'database.yml':
    path   => '/vagrant/config/database.yml',
    source => '/tmp/vagrant-puppet/files/database.yml'
  } ->

  exec { 'rake db':
    path      => '/usr/bin:/usr/local/bin',
    unless    => 'mysql -u root nomade_development',
    command   => 'bundle exec rake db:create:all && bundle exec rake db:migrate && bundle exec rake db:seed && bundle exec rake db:fixtures:load && bundle exec rake db:test:prepare',
    cwd       => '/vagrant',
    logoutput => true,
    user      => 'vagrant',
    require   => Service['mysql']
  } ->

  exec { 'rails server':
    path      => '/usr/bin:/usr/local/bin',
    command   => 'nohup bundle exec rails s > /dev/null 2>&1 &',
    cwd       => '/vagrant',
    logoutput => true,
    user      => 'vagrant'
  }

}