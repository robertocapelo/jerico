# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130509174611) do

  create_table "carta", :force => true do |t|
    t.integer  "iduser"
    t.integer  "idproduct"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "categoria_placas", :force => true do |t|
    t.string   "desc"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "image"
  end

  create_table "fotos", :force => true do |t|
    t.string   "descricao"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "imagem"
    t.integer  "placa_id"
  end

  create_table "imobiliaria", :force => true do |t|
    t.string   "nome"
    t.string   "cnpj"
    t.string   "site"
    t.string   "email"
    t.string   "telefone"
    t.string   "endereco"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "usuario_id"
    t.string   "imagem"
  end

  create_table "observacaos", :force => true do |t|
    t.integer  "placa_id"
    t.text     "conteudo"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "placas", :force => true do |t|
    t.string   "desc"
    t.string   "tipo"
    t.string   "endereco"
    t.float    "longitude"
    t.float    "latitude"
    t.boolean  "gmaps"
    t.datetime "created_at",                                                          :null => false
    t.datetime "updated_at",                                                          :null => false
    t.integer  "tipo_placa_id"
    t.integer  "categoria_placa_id"
    t.integer  "imobiliarium_id"
    t.decimal  "valor",              :precision => 8, :scale => 2
    t.integer  "quartos"
    t.decimal  "condominio",         :precision => 8, :scale => 2
    t.integer  "andar"
    t.integer  "vagas"
    t.boolean  "desabilitado",                                     :default => true
    t.boolean  "ocultar",                                          :default => false
  end

  create_table "tipo_placas", :force => true do |t|
    t.string   "desc"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "usuarios", :force => true do |t|
    t.string   "login"
    t.string   "senha"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
