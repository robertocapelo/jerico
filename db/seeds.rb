# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Usuario.create(login: 'admin', senha: 'matilha')
TipoPlaca.create([{desc: 'todos'},{desc: 'apto'},{desc:'casa'}])
CategoriaPlaca.create([{desc: 'todos'},{desc: 'aluga',image: '/images/alugase.png'},{desc: 'vende',image: '/images/vendese.png'}])
