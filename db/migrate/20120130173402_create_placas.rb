class CreatePlacas < ActiveRecord::Migration
  def change
    create_table :placas do |t|
      t.string :desc
      t.string :tipo
      t.string :endereco
      t.float :longitude
      t.float :latitude
      t.boolean :gmaps

      t.timestamps
    end
  end
end
