class AddQuartosCondominioAndarVagasToPlacas < ActiveRecord::Migration
  def change
  	add_column :placas, :quartos, :int
  	add_column :placas, :condominio, :decimal, :precision => 8, :scale => 2
  	add_column :placas, :andar, :int
  	add_column :placas, :vagas, :int
  end
end
