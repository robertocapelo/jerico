class CreateCategoriaPlacas < ActiveRecord::Migration
  def change
    create_table :categoria_placas do |t|
      t.string :desc

      t.timestamps
    end
  end
end
