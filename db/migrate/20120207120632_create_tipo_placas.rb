class CreateTipoPlacas < ActiveRecord::Migration
  def change
    create_table :tipo_placas do |t|
      t.string :desc

      t.timestamps
    end
  end
end
