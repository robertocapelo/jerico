class AddDesabilitadoToPlacas < ActiveRecord::Migration
  def change
  	add_column :placas, :desabilitado, :boolean, :default => 1
  end
end
