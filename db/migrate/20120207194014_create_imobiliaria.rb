class CreateImobiliaria < ActiveRecord::Migration
  def change
    create_table :imobiliaria do |t|
      t.string :nome
      t.string :cnpj
      t.string :site
      t.string :email
      t.string :telefone
      t.string :endereco

      t.timestamps
    end
  end
end
